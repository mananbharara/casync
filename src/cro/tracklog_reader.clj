(ns cro.tracklog-reader
  (:require [clj-kafka.core :as c :refer [with-resource]]
            [clj-kafka.consumer.zk :as z :refer [consumer messages shutdown]]
            [clojure.core.async :as a :refer [go >!]]))

(defn read-into [config read-channel]
  (go (with-resource [c (consumer config)]
                     shutdown
                     (doseq [n (messages c "y3")]
                       (let [val (String. (:value n) "UTF-8")]
                         (>! read-channel val))))))