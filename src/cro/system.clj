(ns cro.system
  (:require [clojure.core.async :refer [chan <!! go pipeline dropping-buffer]]
            [cro.tracklog-reader :as r]
            [cro.prob-calculator :as p]
            [cro.filter :as f]))

(def config {"zookeeper.connect"  "localhost:2181"
             "group.id"           "tracklog.reader"
             "auto.offset.reset"  "smallest"
             "auto.commit.enable" "false"})

(defn run []
  (let [->tracklog-reader (chan 10)
        tracklog-reader->filter-applier (chan 10)
        dev-null (chan (dropping-buffer 1))]

    (r/read-into config ->tracklog-reader)
    (f/parse-and-filter ->tracklog-reader tracklog-reader->filter-applier)
    (p/model tracklog-reader->filter-applier dev-null)))
