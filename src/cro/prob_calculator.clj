(ns cro.prob-calculator
  (:require [clojure.core.async :refer [pipeline]]
            [taoensso.carmine :as car :refer (wcar)]))

(def redis-conn {:pool {} :spec {:host            "127.0.0.1"
                                 :port            6379
                                 :conn-timeout-ms 2000
                                 :read-timeout-ms 100}})
(defmacro wcar* [& body] `(car/wcar redis-conn ~@body))

(def coefficients
  {:intercept  -8.807771846
   :breakpoint {
                :xl 0.0344782794
                :s  6.8956986462
                }
   :page       {
                :product 1.118604112
                :home    -0.2824746589
                }
   :days       {
                :monday    0
                :tuesday   -0.0029187333
                :wednesday 0.2172381912
                :thursday  0.0764060161
                :friday    0.0322719385
                :saturday  0.0025742235
                :sunday    0.1646458856
                }
   :time       {
                :02-05 0
                :06-10 0.2786993034
                :11-13 0.3343145325
                :14-17 0.3350966116
                :18-22 0.4105185392
                :23-01 0.1839069861
                }
   })

(defn- write-to-redis [visitor-id p]
  (wcar*
    (car/set visitor-id p)
    (car/expire visitor-id (* 30 60))))

(defn- all-factors [{:keys [page weekday daytime breakpoint]}]
  {:intercept  (:intercept coefficients)

   :page       (get-in coefficients [:page (keyword page)])

   :weekday    (get-in coefficients [:days (keyword weekday)])
   :daytime    (get-in coefficients [:time (keyword daytime)])
   :breakpoint (get-in coefficients [:breakpoint (keyword breakpoint)])})

(defn- exponent-value [msg]
  (apply + (vals (all-factors msg))))

(defn- logit [msg]
  (let [exponent (exponent-value msg)]
    (/ 1 (+ 1 (Math/exp (* -1 exponent))))))

(defn- update-prob! [msg]
  (println (logit msg))
  (write-to-redis (:visitorId msg) (logit msg))
  msg)

(defn model [input-chan output-chan]
  (pipeline 4 output-chan (keep update-prob!) input-chan))
