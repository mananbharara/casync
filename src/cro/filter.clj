(ns cro.filter
  (:require [clojure.core.async :refer [pipeline]]
            [clojure.data.json :refer [read-str]]))

(defn- valid-msg-or-nil [msg]
  (if (:productId msg)
    msg
    nil))

(defn- keep-only-product-pages [msg]
  (some-> msg
          (read-str :key-fn keyword)
          (valid-msg-or-nil)))

(defn parse-and-filter [input output]
  (pipeline 4 output (keep keep-only-product-pages) input))
