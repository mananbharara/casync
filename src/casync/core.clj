(ns casync.core
  (:require [clojure.core.async
             :as _
             :refer [>! <! >!! <!! go chan buffer close! thread
                     dropping-buffer sliding-buffer alts! alts!! timeout]]))