(ns casync.uprev
  (:require [clojure.core.async
             :as _
             :refer [>!! >! <! chan go]]))

(defn upper-caser [in]
  (let [out (chan)]
    (go (while true (>! out (clojure.string/upper-case (<! in)))))
    out))

(defn reverser [in]
  (let [out (chan)]
    (go (while true (>! out (clojure.string/reverse (<! in)))))
    out))

(defn printer
  [in]
  (go (while true (println (<! in)))))

(defn start []
  (let [in-chan (chan)
        upper-caser-out (upper-caser in-chan)
        reverser-out (reverser upper-caser-out)]
    (printer reverser-out)
    in-chan))
