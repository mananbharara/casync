(ns casync.alts-example
  (:require [clojure.core.async
             :as _
             :refer [alts!! go chan >! timeout]]))

(defn product-channel [i]
  (let [ch (chan)]
    (go (Thread/sleep (rand 100))
        (>! ch (str "products" i)))
    ch))

(defn timed-get-products []
  (let [chan1 (product-channel 1)
        chan2 (product-channel 2)
        chan3 (product-channel 3)
        [prds ch] (alts!! [chan1 chan2 chan3 (timeout 20)])]
    (if prds
      (println prds)
      (println "TIMEOUT"))
    [chan1 chan2 chan3]))
